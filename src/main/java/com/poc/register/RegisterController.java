package com.poc.register;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController {

	@Autowired
	private RegisterService registerService;

	@GetMapping(value = "/hi")
	public String hi() {
		return "hi amrit!!";
	}

	@GetMapping(value = "/bye")
	public String bye() {
		return "hi TD!!!";
	}

	@GetMapping(value = "/tata")
	public String tata() {
		return "tata";
	}

	@GetMapping(value = "/employees")
	public List<Employee> getAllEmployees() {
		return registerService.getAll();
	}

	@PostMapping(value = "/employees")
	public ResponseEntity<?> addEmployee(@RequestBody Employee employee) {
		registerService.add(employee);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/employees/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id) {
		registerService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
